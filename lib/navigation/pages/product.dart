import 'package:flutter/material.dart';

import './profile.dart';

class ProductPage extends StatelessWidget {
  const ProductPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // secara default jika memakai appbar bawaan, maka halaman kedua terdapat tombol bacl
        // kalau mau hilangin tombol back bisa pake leading sizedbox
        leading: SizedBox(),
        title: Text("Page Product"),
      ),
      body: Center(
        child: Column(
          children: [
            Text("Ini Halaman product"),
            SizedBox(
              height: 50,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text("<< Back page"),
                ),
                ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => ProfilePage(),
                      ),
                    );
                  },
                  child: Text("Next Page >>"),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
