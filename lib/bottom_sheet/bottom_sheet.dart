import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: HomePage());
  }
}

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Bottom Sheet"),
      ),
      body: Center(
        child: Padding(
          padding: EdgeInsets.all(30),
          child: ElevatedButton(
            onPressed: () {
              showModalBottomSheet(
                context: context,
                // isdismisabble untuk jika dibelakangnya bisa di klik atau tidak untuk kembali dari bottom sheet
                // secara default isdismissible adalah true
                isDismissible: false,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                builder: (context) => SizedBox(
                  height: 300,
                  // color: Colors.white,
                  child: ListView(
                    children: [
                      ListTile(
                        leading: Icon(Icons.photo),
                        title: Text("Photo"),
                        onTap: () => print("foto"),
                      ),
                      ListTile(
                        leading: Icon(Icons.music_note_rounded),
                        title: Text("Music"),
                        onTap: () => print("music"),
                      ),
                      ListTile(
                        leading: Icon(Icons.video_collection),
                        title: Text("Video"),
                        onTap: () => print("video"),
                      ),
                      ListTile(
                        leading: Icon(Icons.share),
                        title: Text("Share"),
                        onTap: () => print("share"),
                      ),
                      ListTile(
                        leading: Icon(Icons.cancel),
                        title: Text("Cancel"),
                        onTap: () => Navigator.pop(context),
                      ),
                    ],
                  ),
                ),
              );
            },
            child: Text("Show Bottom Sheet"),
          ),
        ),
      ),
    );
  }
}
