import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Whatsapp"),
          backgroundColor: Colors.teal,
          centerTitle: false,
          bottom: TabBar(
            tabs: [
              Tab(
                icon: Icon(Icons.camera_alt),
              ),
              Tab(
                text: "Chats",
                // child: Text("Chats"),
              ),
              Tab(
                text: "Status",
              ),
              Tab(
                text: "Calls",
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            Center(
              child: Text("Camera"),
            ),
            Center(
              child: Text("Chats"),
            ),
            Center(
              child: Text("Status"),
            ),
            Center(
              child: Text("Calls"),
            ),
          ],
        ),
      ),
    );
  }
}

// kalau tanpa defaulttabcontroller, harus membuat variabel late tabcontroller, dan with SingleTickerProviderStateMixin
class WithoutDefaultTabController extends StatefulWidget {
  const WithoutDefaultTabController({Key? key}) : super(key: key);

  @override
  State<WithoutDefaultTabController> createState() =>
      _WithoutDefaultTabControllerState();
}

class _WithoutDefaultTabControllerState
    extends State<WithoutDefaultTabController>
    with SingleTickerProviderStateMixin {
  late TabController tabC = TabController(length: 4, vsync: this);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Whatsapp"),
        backgroundColor: Colors.teal,
        centerTitle: false,
        bottom: TabBar(
          controller: tabC,
          tabs: [
            Tab(
              icon: Icon(Icons.camera_alt),
            ),
            Tab(
              text: "Chats",
              // child: Text("Chats"),
            ),
            Tab(
              text: "Status",
            ),
            Tab(
              text: "Calls",
            ),
          ],
        ),
      ),
      body: TabBarView(
        controller: tabC,
        children: [
          Center(
            child: Text("Camera"),
          ),
          Center(
            child: Text("Chats"),
          ),
          Center(
            child: Text("Status"),
          ),
          Center(
            child: Text("Calls"),
          ),
        ],
      ),
    );
  }
}
