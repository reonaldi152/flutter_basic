import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // const MyApp({Key? key}) : super(key: key);
  final List data = [
    {
      "judul": "pilihan ke - 1",
      "data": 1,
    },
    {
      "judul": "pilihan ke - 2",
      "data": 2,
    },
    {
      "judul": "pilihan ke - 3",
      "data": 3,
    }
  ];

  late int dataAwal;

  @override
  void initState() {
    dataAwal = data[1]["data"];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dropdown"),
        centerTitle: true,
      ),
      body: Center(
        child: Padding(
          padding: EdgeInsets.all(30),
          child: DropdownButton<int>(
            value: dataAwal,
            items: data
                .map(
                  // e adalah list data yang diatas, yang telah dibuat
                  (e) => DropdownMenuItem(
                    child: Text("${e['judul']}"),
                    // value: "${e['data']}",
                    value: e['data'] as int,
                  ),
                )
                .toList(),
            onChanged: (value) {
              print(value);
            },
          ),
        ),
      ),
    );
  }
}
