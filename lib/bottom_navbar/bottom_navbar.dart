import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late int index;

  List showidget = [
    Center(
      child: Text("HOME"),
    ),
    Center(
      child: Text("CART"),
    ),
    Center(
      child: Text("PROFILE"),
    ),
  ];

  @override
  void initState() {
    index = 0;
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Bottom Navbar"),
      ),
      body: showidget[index],
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.blue,
        // color selected untuk membuat warna pada saat ditekan
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.black26,
        iconSize: 26,
        onTap: (value) {
          // value merupakan index dari bottom navbar
          index = value;
        },
        // jika ingin bottomnavbar nya aktifnya otomatis langsung yg ditengah bisa pake currentindex
        currentIndex: index,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: "home",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_cart),
            label: "cart",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: "profile",
          ),
        ],
      ),
    );
  }
}
